# README #

Temperature Testing 

### What is this repository for? ###

Finding the temperature for all the thermal zones available 

### How do I get set up? ###

*Go to Build Variants in Left side of the screen select Active Build Variant to sbc_debug 

*For 1st time installation 

*Uncomment the .MainActivity section in the AndroidManifest.xml file. 

*Goto Run in menu section select Run -> Edit Configuration -> Launch -> From Nothing to Default Activity -> select apply and Run. 

*From next time to run in background comment .MainActivity and select Nothing in launch section
