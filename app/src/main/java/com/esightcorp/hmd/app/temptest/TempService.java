package com.esightcorp.hmd.app.temptest;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class TempService extends Service {
    private static final String TAG = "TempService";

    long startTime = 0;
    Handler timerHandler = new Handler();
    Runnable runnable;
    private Timer mTimer = null;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    String strDate;
    String read = "";
    public static final long NOTIFY_INTERVAL = 1000;
    private static final String thermalPath = "/sys/class/thermal/";
//    private static ArrayList<Pair> mZoneList = new ArrayList<Pair>();


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate: ");
        BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);
        Log.e(TAG, "Battery Percentage: "+bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY) );
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        String channel_name = "Temperature Service";
        String CHANNEL_ID = "com.esightcorp.hmd.app.TempService";
        int importance = NotificationManager.IMPORTANCE_NONE;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.i(TAG, "Building Notification Channel");
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, channel_name, importance);
            channel.setLightColor(Color.GREEN);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("Overlay is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(23, notification);
        return START_STICKY;
    }

    public class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            timerHandler.post(runnable = new Runnable() {
                @Override
                public void run() {
                    calendar = Calendar.getInstance();
                    simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
                    strDate = simpleDateFormat.format(calendar.getTime());
                    long millis = System.currentTimeMillis() -startTime;
                    long seconds = (long) (millis / 1000);
                    seconds = seconds % 60;
                    if (seconds == 59){

                        Float[] thermalzone = new Float[38];
                        String[] thermaltemp = new String[38];
                        for(int i=0;i<=37;i++){
                            thermalzone[i] = (TestUtils.readFloat(thermalPath+"thermal_zone"+ i +"/temp"));
                            if (thermalzone[i] > 0.f) {
                                if (thermalzone[i] < 100.f)
                                    thermaltemp[i] = String.format("%.0f", thermalzone[i]);
                                else if (thermalzone[i] < 1000.f)
                                    thermaltemp[i] = String.format("%.1f", thermalzone[i]/10.f);
                                else
                                    thermaltemp[i] = String.format("%.3f", thermalzone[i]/1000.f);
                            }
                        }
                        String path = "/sdcard/";
                        File file = new File(path,"Temperature.txt");
                        try {
                            FileOutputStream outputStreamWriter = new FileOutputStream(file,true);
                            for(int i=0;i<=37;i++)
                              outputStreamWriter.write(("Time : " + strDate +" , ThermalZone : "+ i + " , Temperature : " + thermaltemp[i] + "\n" ).getBytes());
                        }
                        catch (IOException e) {
                        }
                    }
                    timerHandler.postDelayed(this,999999999);
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timerHandler.removeCallbacks(runnable);
        mTimer.cancel();
    }
}
