package com.esightcorp.hmd.app.temptest;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TestUtils
{

	public static String read(String filename) {
		try {
			char[] buffer = new char[64];
			FileReader reader = new FileReader(new File(filename));
			try {
				reader.read(buffer, 0, 64);
			} catch (IOException e) {
			} finally {
				reader.close();
			}
			return (new String(buffer)).trim();
		} catch (IOException e) {
		}

		return "";
	}

	public static int readInt(String filename) {
		String temp = read(filename);
		if (temp.isEmpty())
			return Integer.MIN_VALUE;

		int num;
		try {
			num = Integer.parseInt(temp);
		} catch(NumberFormatException e) {
			num = Integer.MIN_VALUE;
		}
		return num;
	}

	public static float readFloat(String filename) {
		String temp = read(filename);
		if (temp.isEmpty())
			return Float.MIN_VALUE;

		float num;
		try {
			num = Float.parseFloat(temp);
		} catch(NumberFormatException e) {
			num = Float.MIN_VALUE;
		}
		return num;
	}
}
